using Microsoft.AspNetCore.Mvc;
using SoftwerkAPI.Interfaces;
using SoftwerkAPI.Models;

namespace SoftwerkAPI.Controllers {

    [Route("api/[controller]")]
    public class PeriodController : Controller {

        public IPeriodRepository<Period> _periodRepository { get; set; }

        public PeriodController(IPeriodRepository<Period> periodRepository) {
            _periodRepository = periodRepository;
        }

        [HttpGet("exists/{itemId}/{year}")]
        public IActionResult Exists(int itemId, int year) {
            return Ok(_periodRepository.HasItem(itemId, year));
        }
    }
}

using Microsoft.AspNetCore.Mvc;
using SoftwerkAPI.Interfaces;
using SoftwerkAPI.Models;

namespace SoftwerkAPI.Controllers {

    [Route("api/[controller]")]
    public class ProjectsController : Controller {

        public IProjectRepository<Project> _projectRepository { get; set; }

        public ProjectsController(IProjectRepository<Project> projectRepository) {
            _projectRepository = projectRepository;
        }
        
        [HttpGet("{page}")]
        public IActionResult GetProjects(int page) {
            if (page == 0) page = 1;
            return Ok(_projectRepository.GetItems(page));
        }

        [HttpGet("id/{projectId}")]
        public IActionResult GetProject(int projectId) {
            return Ok(_projectRepository.GetItem(projectId));
        }

        [HttpPost("add")]
        public IActionResult AddProject([FromBody]Project project) {

            if (project == null || string.IsNullOrEmpty(project.Name)) return BadRequest(new {message = "Invalid parameters"});

            var itemId = _projectRepository.Create(project);
            return Ok(itemId);
        }
    }
}
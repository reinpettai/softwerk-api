using System.Linq;
using Microsoft.AspNetCore.Mvc;
using SoftwerkAPI.Interfaces;
using SoftwerkAPI.Models;

namespace SoftwerkAPI.Controllers {

    [Route("api/[controller]")]
    public class ItemsController : Controller {

        public IItemRepository<Item> _itemRepository { get; set; }
        public IPeriodRepository<Period> _periodRepository { get; set; }

        public ItemsController(IItemRepository<Item> itemRepository, IPeriodRepository<Period> periodRepository) {
            _itemRepository = itemRepository;
            _periodRepository = periodRepository;
        }
        
        [HttpGet("{id}/{year}/{page}")]
        public IActionResult GetProjectItems(int id, int year, int page) {
            if (page == 0) page = 1;
            var result = _itemRepository.GetItems(id, year, page);
            return Ok(result);
        }

        [HttpGet("{id}/{year}")]
        public IActionResult GetProjectItem(int id, int year) {
            return Ok(_itemRepository.GetItem(id, year));
        }

        [HttpPost("add")]
        public IActionResult AddProjectItem([FromBody]Item projectItem) {

            if (projectItem != null) {

                var periods = projectItem.Periods.Select(i => new Period() {
                    Year = i.Year,
                    Amount = i.Amount,
                    Price = i.Price
                }).ToList();

                var item = new Item() {
                    Name = projectItem.Name,
                    ProjectId = projectItem.ProjectId,
                    Periods = periods
                };

                var itemId = _itemRepository.Create(item);
                return Ok(itemId);
            }
            else return BadRequest("Invalid parameters");

        }

        [HttpPost("copy")]
        public IActionResult CopyItems([FromBody]int[] items) {
            var list = _itemRepository.Copy(items);
            return Ok(list);
        }

        [HttpGet("{id}/{year}/search/{param}")]
        public IActionResult SearchByString(int id, int year, string param) {
            if (!string.IsNullOrEmpty(param)) {
                return Ok(_itemRepository.Search(id, year, param));
            }
            return Ok();
        }

        [HttpDelete("remove")]
        public IActionResult RemoveProjectItem([FromBody]int[] ids) {
            _itemRepository.Remove(ids);
            return Ok("Items deleted");
        }

        [HttpPost("{id}/periods/add")]
        public IActionResult AddProjectItemPeriod(int id, int year, int amount, decimal price) {

            var periodId = _periodRepository.Create(new Period() {
                ItemId = id,
                Year = year,
                Amount = amount,
                Price = price
            });

            return Ok(periodId);
        }
        
    }
}
﻿using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.DependencyInjection;
using SoftwerkAPI.Interfaces;
using SoftwerkAPI.Models;
using SoftwerkAPI.Repositories;
using SoftwerkAPI.Services;

namespace SoftwerkAPI
{
    public class Startup
    {
        // This method gets called by the runtime. Use this method to add services to the container.
        // For more information on how to configure your application, visit https://go.microsoft.com/fwlink/?LinkID=398940
        public void ConfigureServices(IServiceCollection services)
        {
            var connectionString = "Server=localhost;Database=Test;User Id=sa;Password=Passw0rd!";
            services.AddDbContext<DbContextClass>(x => x.UseSqlServer(connectionString));
            services.AddScoped<IProjectRepository<Project>, ProjectRepository>();
            services.AddScoped<IItemRepository<Item>, ItemRepository>();
            services.AddScoped<IPeriodRepository<Period>, PeriodRepository>();
            services.AddCors();
            services.AddMvc().AddJsonOptions(
                options => options.SerializerSettings.ReferenceLoopHandling =            
                Newtonsoft.Json.ReferenceLoopHandling.Ignore
            );
        }

        // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
        public void Configure(IApplicationBuilder app, IHostingEnvironment env, DbContextClass dbContext)
        {
            if (env.IsDevelopment())
            {
                app.UseDeveloperExceptionPage();
            }

            app.UseStaticFiles();

            dbContext.CreateMockData();

            app.UseCors(builder => {
                builder.AllowAnyOrigin();
                builder.AllowAnyHeader();
                builder.AllowAnyMethod();
            });
            
            app.UseMvc();
        }
    }
}

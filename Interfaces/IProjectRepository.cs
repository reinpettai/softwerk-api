
using System.Threading.Tasks;
using SoftwerkAPI.Models;

namespace SoftwerkAPI.Interfaces {

    public interface IProjectRepository<T>
    {
        int Create(T item);
        Task<ItemsResponse> GetItems(int pageIndex);
        Project GetItem(int projectId);
    }
}

using System.Collections;
using System.Threading.Tasks;
using SoftwerkAPI.Models;

namespace SoftwerkAPI.Interfaces {

    public interface IItemRepository<T>
    {
        int Create(T item);
        IEnumerable Copy(int[] ids);
        void Remove(int id);
        void Remove(int[] id);
        Task<ItemsResponse> GetItems(int? id, int? year, int pageIndex);
        IEnumerable Search(int id, int year, string param);
        T GetItem(int id, int? year);
    }
}
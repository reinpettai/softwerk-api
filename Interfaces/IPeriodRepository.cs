
namespace SoftwerkAPI.Interfaces {

    public interface IPeriodRepository<T>
    {
        int Create(T item);
        bool HasItem(int itemId, int year);
        void Remove(int id);
    }
}
using System;
using System.Collections.Generic;
using System.Linq;
using SoftwerkAPI.Models;

namespace SoftwerkAPI.Services {

    public static class DbContextClassExtensions {
        
        public static void CreateMockData(this DbContextClass context) {

            int projectCount = 100;
            int itemCount = 10000;

            if (!context.Projects.Any()) {

                var projects = new List<Project>();

                for (var i = 0; i < projectCount; i++) {
                    projects.Add(new Project() {
                        Name = "Hankekava nr " + i
                    });
                }

                context.Projects.AddRange(projects);
                context.SaveChanges();
            }

            if (!context.Items.Any()) {

                var items = new List<Item>();
                var rnd = new Random();

                for (var i = 0; i < itemCount; i++) {

                    items.Add(new Item() {
                        Name = "Hankeobjekt nr " + i,
                        ProjectId = rnd.Next(1,projectCount),
                        Periods = getPeriods()
                    });
                }

                context.Items.AddRange(items);
                context.SaveChanges();
            }
        }

        public static ICollection<Period> getPeriods() {

            var periods = new List<Period>();
            var rnd = new Random();

            for (var i = 0; i < rnd.Next(1,10); i++) {

                periods.Add(new Period() {
                    Year = rnd.Next(2014,2045),
                    Amount = rnd.Next(1,200),
                    Price = rnd.Next(100, 200000)
                });
            }

            return periods;
        }
    }
}
using Microsoft.EntityFrameworkCore;
using SoftwerkAPI.Models;

namespace SoftwerkAPI.Services
{
     public class DbContextClass : DbContext
     {
          public DbSet<Project> Projects { get; set; }
          public DbSet<Item> Items { get; set; }
          public DbSet<Period> Periods { get; set; }
          public DbContextClass(
               DbContextOptions<DbContextClass> options)
               : base(options)
          {
               Database.EnsureCreated();
          }
     }
}
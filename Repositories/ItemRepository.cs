using System.Collections;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.EntityFrameworkCore;
using SoftwerkAPI.Helpers;
using SoftwerkAPI.Interfaces;
using SoftwerkAPI.Models;
using SoftwerkAPI.Services;

namespace SoftwerkAPI.Repositories {

    public class ItemRepository : IItemRepository<Item>
    {
        private DbContextClass _context;

        public ItemRepository(DbContextClass context)
        {
            _context = context;
        }

        public IEnumerable Copy(int[] ids)
        {
            var copyItems = _context.Items.Where(i => ids.Contains(i.ItemId)).Select(i => new Item() {
                Name = i.Name,
                Periods = i.Periods.Select(x => new Period() {
                    Year = x.Year,
                    Price = x.Price,
                    Amount = x.Amount
                }).ToList(),
                ProjectId = i.ProjectId
            }).ToList();

            _context.Items.AddRange(copyItems);
            _context.SaveChanges();
            return copyItems.Select(i => i.ItemId).ToList();
        }

        public int Create(Item item)
        {
            _context.Items.Add(item);
            _context.SaveChanges();
            return item.ItemId;
        }

        public Item GetItem(int id, int? year)
        {
            return _context.Items.Where(i => i.ItemId == id).Select(i => new Item() {
                ItemId = i.ItemId,
                Name = i.Name,
                Periods = i.Periods.Where(p => p.Year >= year).ToList()
            }).FirstOrDefault();
        }

        public async Task<ItemsResponse> GetItems(int? id, int? year, int pageIndex)
        {
            int pageSize = 50;

            var items = _context.Items.Where(p => p.ProjectId == id).OrderBy(x => x.ItemId).Select(x => new Item() {
                ItemId = x.ItemId,
                Name = x.Name,
                Periods = x.Periods.Where(i => i.Year >= (int)year).Take(5).ToList()
            });

            var result = await PaginatedList<Item>.CreateAsync(items, pageIndex, pageSize);

            return new ItemsResponse() {
                Items = result,
                TotalPages = result.TotalPages
            };
        }

        public void Remove(int id)
        {
            var item = _context.Items.Where(i => i.ItemId == id).First();
            if (item == null) return;
            _context.Items.Remove(item);
            var periods = _context.Periods.Where(i => i.ItemId == id);
            if (periods.Count() <= 0) return;
            _context.Periods.RemoveRange(periods);
            _context.SaveChanges();
        }

        public void Remove(int[] ids)
        {
            var items = _context.Items.Where(i => ids.Contains(i.ItemId)).ToList();
            _context.Items.RemoveRange(items);
            _context.SaveChanges();
        }

        public IEnumerable Search(int id, int year, string param)
        {
            return _context.Items.Where(s => s.ProjectId == id && s.Name.Contains(param)).Take(100).OrderBy(i => i.ItemId).Select(x => new Item() {
                ItemId = x.ItemId,
                Name = x.Name,
                Periods = x.Periods.Where(i => i.Year >= year).Take(5).ToList()
            });
        }
    }
}
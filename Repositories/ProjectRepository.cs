using System.Collections;
using System.Linq;
using System.Threading.Tasks;
using SoftwerkAPI.Helpers;
using SoftwerkAPI.Interfaces;
using SoftwerkAPI.Models;
using SoftwerkAPI.Services;

namespace SoftwerkAPI.Repositories {

    public class ProjectRepository : IProjectRepository<Project>
    {
        private DbContextClass _context;

        public ProjectRepository(DbContextClass context)
        {
            _context = context;
        }

        public int Create(Project item)
        {
            _context.Projects.Add(item);
            _context.SaveChanges();
            return item.ProjectId;
        }

        public Project GetItem(int projectId)
        {
            return _context.Projects.Where(i => i.ProjectId == projectId).First();
        }

        public async Task<ItemsResponse> GetItems(int pageIndex)
        {
            int pageSize = 50;
            var items = _context.Projects;
            var result = await PaginatedList<Project>.CreateAsync(items, pageIndex, pageSize);
            return new ItemsResponse() {
                Items = result,
                TotalPages = result.TotalPages
            };
        }

    }
}
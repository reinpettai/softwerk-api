using System.Collections;
using System.Linq;
using System.Threading.Tasks;
using SoftwerkAPI.Interfaces;
using SoftwerkAPI.Models;
using SoftwerkAPI.Services;

namespace SoftwerkAPI.Repositories {

    public class PeriodRepository : IPeriodRepository<Period>
    {

        private DbContextClass _context;

        public PeriodRepository(DbContextClass context)
        {
            _context = context;
        }

        public int Create(Period item)
        {
            _context.Periods.Add(item);
            _context.SaveChanges();
            return item.PeriodId;
        }

        public bool HasItem(int itemId, int year)
        {
            return _context.Periods.Any(i => i.ItemId == itemId && i.Year == year);
        }
    
        public void Remove(int id)
        {
            var item = _context.Periods.Where(i => i.PeriodId == id).First();
            if (item == null) return;
            _context.Periods.Remove(item);
        }

    }
}
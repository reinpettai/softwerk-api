using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using Newtonsoft.Json;

namespace SoftwerkAPI.Models {

    public class Item {

        public Item() {
            this.Periods = new List<Period>();
        }

        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int ItemId { get; set; }

        [Required]
        public string Name { get; set; }

        [Required]
        public int ProjectId { get; set; }
        public Project Project { get; set; }

        public virtual ICollection<Period> Periods { get; set; }
        
    }
}
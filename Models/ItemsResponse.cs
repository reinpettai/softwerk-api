using System.Collections;

namespace SoftwerkAPI.Models {

    public class ItemsResponse {

        public IEnumerable Items { get; set;}
        public int TotalPages { get; set; }
    }
}
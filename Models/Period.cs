using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace SoftwerkAPI.Models {

    public class Period {

        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int PeriodId { get; set; }

        [Required]
        public int Year { get; set; }

        [Required]
        public int Amount { get; set; }

        [Required, Column(TypeName = "decimal(18,2)")]
        public decimal Price { get; set; }

        public int ItemId { get; set; }
        public virtual Item Item { get; set; }
    }
}